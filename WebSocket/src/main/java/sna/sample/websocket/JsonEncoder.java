/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sna.sample.websocket;

import javax.json.Json;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author baolu
 */
public class JsonEncoder implements Encoder.Text<Record>{
    @Override
    public void init(final EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public String encode(Record record) throws EncodeException {
        return Json.createObjectBuilder()
                .add("time", record.getTime())
                .add("name", record.getName())
                .toString();
    }
}
