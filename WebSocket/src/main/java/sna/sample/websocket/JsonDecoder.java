/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sna.sample.websocket;

import java.io.StringReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author baolu
 */
public class JsonDecoder implements Decoder.Text<Record>{
    @Override
    public void init(final EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public Record decode(final String textMessage) throws DecodeException {
        Record record = new Record();
        JsonObject obj = Json.createReader(new StringReader(textMessage))
                .readObject();
        record.setTime(obj.getString("time"));
        record.setName(obj.getString("name"));
        
        return record;
    }

    @Override
    public boolean willDecode(final String s) {
        return true;
    }
}
