/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sna.sample.websocket;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author baolu
 */
@ServerEndpoint(value = "/MainServer", encoders = JsonEncoder.class, decoders = JsonDecoder.class)
public class MainServerWSEndpoint {

    @OnOpen
    public void onOpen() {
        System.out.println("Open Connection ...");
    }

    @OnClose
    public void onClose() {
        System.out.println("Close Connection ...");
    }

    @OnMessage
    public String onMessage(Session session, Record message) throws IOException, ParseException {
        System.out.println("Welcome: " + message.getName());
        String mess = "Welcome: " + message.getName();
        mess += "! " + checkTime(message);
        for (Session sess : session.getOpenSessions()) {
            if (sess.isOpen()) {
                sess.getBasicRemote().sendText(mess);
            }
        }
        System.out.println(new Date());
        return mess;
    }

    @OnError
    public void onError(Throwable e) {
    }

    private String checkTime(Record record) throws ParseException {
        if (record != null) {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startWork = new Date();
            Date currentTime = parser.parse(record.getTime());

            // check is same day
            boolean isSameDayWork = (startWork.getDate() == currentTime.getDate());
            boolean isSameMonth = (startWork.getMonth() == currentTime.getMonth());
            boolean isSameYear = (startWork.getYear() == currentTime.getYear());

            if (isSameDayWork && isSameMonth && isSameYear) {
                startWork = new Date(startWork.getYear(), startWork.getMonth(), startWork.getDate(), 8, 0, 0);
                startWork = parser.parse(parser.format(startWork));
                
                // check before or after 8:00 AM
                if (currentTime.compareTo(startWork) <= 0) {
                    return "You are present";
                } else {
                    return "You are late";
                }
            } else {
                return "Invalid Data";
            }
        }
        return "";
    }
}
