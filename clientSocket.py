import asyncio
import websockets
import time

async def hello():
    uri = "ws://localhost:8080/WebSocket/MainServer"
    async with websockets.connect(uri) as websocket:
        list = ['{"time":"2020-04-13 07:01:00.00", "name":"Thien Bao"}', '{"time":"2020-04-13 08:01:00.00", "name":"Ikko"}', '{"time":"2020-02-27 07:59:30.00", "name":"Ross"}', '{"time":"2020-02-27 08:00:30.00", "name":"Tae"}']
        for x in list:
            time.sleep(1)
            await websocket.send(x)
        pass

asyncio.get_event_loop().run_until_complete(hello())
